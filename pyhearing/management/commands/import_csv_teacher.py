# coding: utf-8
import sys
from random import choice
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
import hearings.models as hmodels
import csv
import itertools
class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        if len(args) == 0:
            print "Must give a file name!"
            sys.exit(2)
        try:
            infd = open(args[0])
        except Exception, e:
            print "Error", e
            sys.exit(2)

        reader = csv.reader(infd)
        head = reader.next()
        def find_class(name):
            try:
                my_class = hmodels.SchoolClass.objects.get(name=name)

            except:
                # don't exists, create it
                
                my_class = hmodels.SchoolClass(name=name)
                my_class.save()
                print "Created class ", my_class
            return my_class
            
        for row in reader:
            rd = dict(itertools.izip(head, row))
            my_class = find_class(rd['Classe'])

            try:
                user = User.objects.get(username=rd['Username'])
            except:
                # don't exists
                user = User(username=rd['Username'],
                            email=rd['Email'],
                            first_name=rd['Nome'],
                            last_name=rd['Cognome'])
                user.save()
                print "Created user", user

            hmodels.Teacher(user=user,
                            school_class=my_class,
                            subject_taught = rd['Materia']
                            ).save()
            
            print user, "added to class", my_class
