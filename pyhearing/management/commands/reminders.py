# coding: utf-8
from random import choice
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
import hearings.models as hmodels
import datetime

class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        # send reminders to parents
        

        appointments = hmodels.Appointment.objects.filter(when__day = tomorrow)
        for h in appointments:
            print "Sending reminders to parent for %s" % tomorrow
            h.send_parent_reminder()

        # send reminders to teacher
        for t in hmodels.Teacher.objects.all():
            apps = t.appointments_as_teacher.filter(when__day=tomorrow)
            if apps:
                print "Sending reminder to teacher %s" % t
                t.send_teacher_reminder(apps)
            
        print "Done!"
