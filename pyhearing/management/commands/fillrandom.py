# coding: utf-8
from random import choice
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
import hearings.models as hmodels


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        print "Creating 10 classes..."
        schoolclasses = []
        for i in range(10):
            schoolclass = hmodels.SchoolClass(
                name="SchoolClass %02d" % i,
            )
            schoolclass.save()
            schoolclasses.append(schoolclass)
        print "Creating 10 teachers, with 2 classes each..."
        teachers = []
        for i in range(10):
            username = "teacher%02d" % i
            johndoe = User(
                username=username,
                first_name='John %02d' % i,
                last_name='Doe %02d' % i,
                email='teacher%02d@email.it.invalid' % i,
                is_active=True,
            )
            johndoe.set_password(johndoe.username)
            johndoe.save()
            for j in range(2):
                teacher = hmodels.Teacher(
                    user=johndoe,
                    school_class=choice(schoolclasses),
                    subject_taught="LaLaLaLa, BlaBlaBla",
                )
                teacher.save()
                teachers.append(teacher)
        print "Creating 30 parents..."
        parents = []
        for i in range(30):
            username = "parent%02d" % i
            jackdoe = User(
                username=username,
                first_name='Jack %02d' % i,
                last_name='Doe %02d' % i,
                email='parent%02d@email.it.invalid' % i,
                is_active=True,
            )
            jackdoe.set_password(jackdoe.username)
            jackdoe.save()
            parent = hmodels.Parent(
                user=jackdoe,
                school_class=choice(schoolclasses),
            )
            parent.save()
            parents.append(parent)
        print "Done!"
