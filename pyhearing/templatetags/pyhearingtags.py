from django import template
from pyhearing.views import _is_teacher, _is_parent

register = template.Library()

register.filter('is_teacher', _is_teacher)
register.filter('is_parent', _is_parent)
