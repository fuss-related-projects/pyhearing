# coding: utf-8
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from django import forms
import post_office.models as pomodels


def login_view(request):
    username = password = ''
    _next = request.GET.get("next", None)
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                messages.info(request, _('Welcome, %s!' % username))
                if _next:
                    return redirect(_next)
                else:
                    return redirect('home')
            else:
                messages.error(request, _('Account %s is disabled' % username))
        else:
            messages.error(request,
                _("The credentials you supplied were not correct or did not grant access to this resource."))
    return render(request, 'login.html')


@login_required
def logout_view(request):
    logout(request)
    return redirect('home')


def _is_teacher(user):
    return True if len(user.as_teacher.all()) > 0 else False


def _is_parent(user):
    return True if len(user.as_parent.all()) > 0 else False


@login_required
def home(request):
    return redirect('personal_page')


class EmailForm(forms.Form):
    subject = forms.CharField(max_length=255)
    message = forms.CharField(widget=forms.Textarea())

@login_required
def mass_mail(request):
    def validate_email(address):
        from django import forms
        f = forms.EmailField()
        try:
            cleaned_address = f.clean(address)
            return cleaned_address
        except:
            return None

    from post_office import mail
    from django.conf import settings
    # refs #26: check that the user is staff and superuser, to be safe.
    if request.user.is_staff and request.user.is_superuser:
        if request.method == "POST":
            form = EmailForm(request.POST)
            if form.is_valid():
                users = User.objects.all()
                subject = form.cleaned_data['subject']
                message = form.cleaned_data['message']
                how_many = 0
                for user in users:
                    address = validate_email(user.email)
                    if address:
                        mail.send(
                            [address],
                            settings.DEFAULT_FROM_EMAIL,
                            subject=subject,
                            message=message,
                            priority='medium',
                        )
                        how_many += 1
                messages.info(request, _("Queued %(num)d emails (%(num_users)d users)" % dict(num=how_many, num_users=users.count())))
                return redirect('mass_mail')
            else:
                messages.error(request, _("Form not valid"))
        else:
            form = EmailForm()
    else:
        messages.error(request, _("Authorization denied: you cannot send mass mail."))
        return redirect('home')
    return render(request, 'mass_mail.html', {
        'form': form,
    })
