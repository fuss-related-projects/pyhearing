from django.conf.urls import patterns, include, url
from django.contrib import admin
import django.contrib.auth.views as aviews

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'pyhearing.views.home', name="home"),
    url(r'^login/$', 'pyhearing.views.login_view', name="login"),
    url(r'^logout/$', 'pyhearing.views.logout_view', name="logout"),
    url(r'^mass_mail/$', 'pyhearing.views.mass_mail', name='mass_mail'),

    url(r'^hearings/', include('hearings.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^user/password/change/$', 'django.contrib.auth.views.password_change',
        {'template_name': 'password_change_form.html'},
        name="password_change"),
    url(r'^user/password/change/done/$', aviews.password_change_done,
        {'template_name': 'password_change_done.html'}),
    url(r'^user/password/reset/$', aviews.password_reset,
        {'post_reset_redirect' : '/user/password/reset/done/',
        'template_name': 'password_reset_form.html'},
        name="password_reset"),
    url(r'^user/password/reset/done/$',
        aviews.password_reset_done,
        {'template_name': 'password_reset_done.html'}),
    url(r'^user/password/reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
        aviews.password_reset_confirm,
        {'post_reset_redirect' : '/user/password/done/',
        'template_name': 'password_reset_confirm.html'}),
    url(r'^user/password/done/$', aviews.password_reset_complete,
        {'template_name': 'password_reset_complete.html'}),
)
