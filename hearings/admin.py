from django.contrib import admin
import hearings.models as hmodels
from django.contrib.auth.models import User, Group

admin.site.register(hmodels.SchoolClass)



class AppointmentAdmin(admin.ModelAdmin):
    search_fields = ['parent__user__username',
                     'parent__user__first_name',
                     'parent__user__last_name',
                     'teacher__user__username',
                     'teacher__user__first_name',
                     'teacher__user__last_name']
    list_display = ['parent', 'teacher', 'when']
    list_display_links = list_display
admin.site.register(hmodels.Appointment, AppointmentAdmin)

class AvailabilityAdmin(admin.ModelAdmin):
    search_fields = ['teacher__username',
                     'teacher__last_name',
                     'teacher__first_name'
                     ]

    list_display = ['teacher','day','start','end','max_visits']
    list_display_links = list_display
admin.site.register(hmodels.Availability, AvailabilityAdmin)

class TeacherAdmin(admin.ModelAdmin):
    search_fields = ['user__first_name',
                     'user__last_name',
                     'user__email',
                     'user__username',
                     'school_class__name']
    list_display = ['user', 'school_class', 'subject_taught']
admin.site.register(hmodels.Teacher, TeacherAdmin)

class ParentAdmin(admin.ModelAdmin):
    search_fields = ['user__first_name',
                     'user__last_name',
                     'user__email',
                     'user__username',
                     'school_class__name']
    list_display = ['user', 'school_class']
admin.site.register(hmodels.Parent, ParentAdmin)
