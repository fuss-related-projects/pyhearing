from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^personal_page/$', 'hearings.views.personal_page', name='personal_page'),
    url(r'^recap/$', 'hearings.views.recap', name='recap'),
    url(r'^teacher/$', 'hearings.views.teacher', name='teacher'),
                       
    url(r'^parent/$', 'hearings.views.parent', name='parent'),
    url(r'^book_appointment/(?P<id>[0-9]+)?$', 'hearings.views.book_appointment', name='book_appointment'),
    url(r'^save_appointment/(?P<tid>[0-9]+)/(?P<aid>[0-9]+)/$', 'hearings.views.save_appointment', name='save_appointment'),
    url(r'^cancel_appointment/(?P<id>[0-9]+)/$', 'hearings.views.cancel_appointment', name='cancel_appointment'),
    url(r'^single_availability/(?P<id>[0-9]+)?$', 'hearings.views.single_availability', name='single_availability'),
    url(r'^global_availabilities/$', 'hearings.views.global_availabilities', name='global_availabilities'),
    url(r'^delete_availability/(?P<id>[0-9]+)?$', 'hearings.views.delete_availability', name='delete_availability'),
)
