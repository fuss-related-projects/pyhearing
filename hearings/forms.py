# coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _
import hearings.models as hmodels


class SingleAvailabilityForm(forms.ModelForm):
    class Meta:
        model = hmodels.Availability
        exclude = ('teacher',)

    def __init__(self, *args, **kwargs):
        super(SingleAvailabilityForm, self).__init__(*args, **kwargs)
        self.fields['day'] = forms.DateField(error_messages={'required': _("You have to insert the day")})

    def clean(self):
        cleaned_data = super(SingleAvailabilityForm, self).clean()
        start = cleaned_data.get('start', None)
        end = cleaned_data.get('end', None)
        if start and end and start >= end:
            msg = _("Start cannot be after end")
            self._errors['start'] = forms.util.ErrorList([msg])
            self._errors['end'] = forms.util.ErrorList([msg])
            del self.cleaned_data['start']
            del self.cleaned_data['end']
        return self.cleaned_data


class GlobalAvailabilitiesForm(forms.Form):
    first_day = forms.DateField(label=_("Date from"))
    last_day = forms.DateField(label=_("Date to"))
    day = forms.ChoiceField(label=_("Day of week"),
                            choices=(
        (0, _('Monday')),
        (1, _('Tuesday')),
        (2, _('Wednesday')),
        (3, _('Thursday')),
        (4, _('Friday')),
        (5, _('Saturday')),
        #(6, _('Sunday')),
    ))
    start = forms.TimeField(label=_("Start time"))
    end = forms.TimeField(label=_("End time"))
    max_visits = forms.IntegerField(label=_("Max appointments"))

    def clean(self):
        cleaned_data = super(GlobalAvailabilitiesForm, self).clean()
        first_day = cleaned_data.get('first_day', None)
        last_day = cleaned_data.get('last_day', None)
        start = cleaned_data.get('start', None)
        end = cleaned_data.get('end', None)
        # Check days...
        if first_day and last_day and first_day > last_day:
            msg = _("First day cannot be after last day")
            self._errors['first_day'] = forms.util.ErrorList([msg])
            self._errors['last_day'] = forms.util.ErrorList([msg])
            del self.cleaned_data['first_day']
            del self.cleaned_data['last_day']
        # Check times...
        if start and end and start > end:
            msg = _("Start cannot be after end")
            self._errors['start'] = forms.util.ErrorList([msg])
            self._errors['end'] = forms.util.ErrorList([msg])
            del self.cleaned_data['start']
            del self.cleaned_data['end']
        return self.cleaned_data
