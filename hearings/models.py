from datetime import date
from django.db import models
from django.contrib.auth.models import User, Group
from django.core.mail import send_mail
from django.conf import settings
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context


class SchoolClass(models.Model):
    name = models.CharField(max_length=16, unique=True)

    class Meta:
        verbose_name_plural = "School classes"
        ordering = ('name',)

    def __unicode__(self):
        return self.name

class Teacher(models.Model):
    user = models.ForeignKey(User, null=False, blank=False,
                             related_name="as_teacher")
    school_class = models.ForeignKey(SchoolClass, null=False, blank=False,
                                     related_name='teachers')
    subject_taught = models.CharField(max_length=1024)

    def __unicode__(self):
        return "%s %s - %s" % (self.user.first_name, self.user.last_name, self.school_class)

    def get_full_name(self):
        return self.user.get_full_name()

    def can_add_appointment(self):
        availabilities = self.user.availability.all()
        for av in availabilities:
            if av.can_add_appointment():
                return True
        return False

    def send_teacher_reminder(self, appointments):
        message = """
Gent. %s %s,

le ricordiamo i suoi appuntamenti prenotati per le udienze previste
per la giornata di domani:

""" % (self.user.first_name, self.user.last_name)
        
        for a in appointments:
            message += "%s: dalle %s alle %s\n" % (a.parent, a.when.start, a.when.end)

        message += "\nLe auguriamo buon lavoro.\n"
        message = "%s\n\n%s" % (message, settings.NOTIFICATION_EMAIL_SIGNATURE)
        if self.user.email:
            send_mail(settings.NOTIFICATION_EMAIL_SUBJECT,
                      message,
                      settings.NOTIFICATION_EMAIL_FROM,
                      [self.user.email])

class Parent(models.Model):
    user = models.ForeignKey(User, null=False, blank=False,
                             related_name="as_parent")
    school_class = models.ForeignKey(SchoolClass, null=False, blank=False,
                                     related_name='parents')
    class Meta:
        unique_together = ('user', 'school_class')
        
    def get_teachers_appointments(self):
        teachers = []
        for appointment in self.appointments_as_parent.all().filter(when__day__gte=date.today()):
            teachers.append(appointment.when.teacher)
        return teachers

    def get_availabilities_appointments(self):
        availabilities = []
        for appointment in self.appointments_as_parent.all().filter(when__day__gte=date.today()):
            availabilities.append(appointment.when)
        return availabilities

    def __unicode__(self):
        return "%s %s - %s" % (self.user.first_name, self.user.last_name,
                               self.school_class)

    def get_full_name(self):
        return self.user.get_full_name()


class Availability(models.Model):
    teacher = models.ForeignKey(User, null=False, blank=False,
                                related_name="availability")
    day = models.DateField(verbose_name=_("Day"))
    start = models.TimeField(verbose_name=_("Start time"))
    end = models.TimeField(verbose_name=_("End Time"))
    max_visits = models.IntegerField(verbose_name=_("Max appointments"))

    class Meta:
        verbose_name_plural = "Availabilities"
        ordering = ('day', 'start')

    def get_free_slots(self):
        return self.max_visits - self.appointments.all().count()

    def can_add_appointment(self):
        return True if self.get_free_slots() > 0 else False

    def __unicode__(self):
        return "%s - %s (%s - %s)" % (self.teacher,
                                      self.day,
                                      self.start, self.end)

    def _send_boss_email(self, message):
        message = "%s\n\n%s" % (message, settings.NOTIFICATION_EMAIL_SIGNATURE)
        if settings.NOTIFICATION_BOSS_EMAIL:
            send_mail(settings.NOTIFICATION_BOSS_SUBJECT,
                      message,
                      settings.NOTIFICATION_EMAIL_FROM,
                      [settings.NOTIFICATION_BOSS_EMAIL])

    def send_boss_delete(self):
        message = """
Il docente %s ha annullato le udienze previste per il giorno
%s dalle ore %s alle ore %s.
        """ % (self.teacher, self.day,
               self.start, self.end)
        
        self._send_boss_email(message)
    
@receiver(post_delete, sender=Availability)
def notify_boss_delete(sender, **kwargs):
    kwargs['instance'].send_boss_delete()


class Appointment(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    teacher = models.ForeignKey(Teacher, null=False, blank=False,
                                related_name="appointments_as_teacher")
    parent = models.ForeignKey(Parent, null=False, blank=False,
                               related_name="appointments_as_parent")
    when = models.ForeignKey(Availability, null=False, blank=False,
                             related_name="appointments")

    @property
    def iso_timestamp(self):
        return "%sT%s+01:00" % (
            self.when.day.strftime("%Y-%m-%d"),
            self.when.start.strftime("%H:%M:%S")
            )
    
    class Meta:
        ordering = ('created', 'teacher', 'parent', 'when')

    def __unicode__(self):
        return "%s - %s - %s - %s" % (self.parent, self.teacher,
                                      self.when.day, self.when.start)

    def send_boss_notification(self, action):
        message = """

        """

        
    def send_parent_delete(self):
        message = """
Gent. %s,

La informiamo che il suo appuntamento per il giorno %s
con il docente %s e' stato annullato.

Cordiali saluti

""" % (
            self.parent,
            self.when.day.strftime("%d/%m/%Y"),
            self.teacher
            )
        self._send_parent_email(message)


    def send_parent_updates(self):
        message = """
Gent. %s,

La informiamo che il suo appuntamento e' stato modificato,
di seguito i nuovi dettagli.

Nuovo appuntamento per il giorno giorno %s
dalle ore %s alle ore %s, con il nostro docente %s.

Cordiali saluti

""" % (
            self.parent,
            self.when.day.strftime("%d/%m/%Y"),
            str(self.when.start)[:5],
            str(self.when.end)[:5],
            self.teacher
            )
        self._send_parent_email(message)

    def send_parent_created(self):
        plaintext = get_template("hearings/parent_reminder.txt")
        htmltext = get_template("hearings/parent_reminder.html")
        ctx = dict(parent=self.parent,
                   when=self.when,
                   teacher=self.teacher,
                   appointment=self)
        d = Context(ctx)

        if self.parent.user.email:
            subject = settings.NOTIFICATION_EMAIL_SUBJECT
            from_email = settings.NOTIFICATION_EMAIL_FROM
            to = self.parent.user.email
            text_content = plaintext.render(d)
            html_content = htmltext.render(d)
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
                                     

    def send_parent_reminder(self):
        message = """
Gent. %s,

Le ricordiamo il suo appuntamento per il giorno %s
dalle ore %s alle ore %s, con il nostro docente %s.

Cordiali saluti

""" % (
            self.parent,
            self.when.day.strftime("%d/%m/%Y"),
            str(self.when.start)[:5],
            str(self.when.end)[:5],
            self.teacher
            )
        self._send_parent_email(message)


    def _send_parent_email(self, message):
        message = "%s\n\n%s" % (message, settings.NOTIFICATION_EMAIL_SIGNATURE)
        if self.parent.user.email:
            send_mail(settings.NOTIFICATION_EMAIL_SUBJECT,
                      message,
                      settings.NOTIFICATION_EMAIL_FROM,
                      [self.parent.user.email])

@receiver(post_save, sender=Appointment)
def notify_updates(sender, **kwargs):
    if kwargs.get('created'):
        kwargs['instance'].send_parent_created()
    else:
        kwargs['instance'].send_parent_updates()

@receiver(post_delete, sender=Appointment)
def notify_delete(sender, **kwargs):
    kwargs['instance'].send_parent_delete()

