# coding: utf-8
from datetime import time, timedelta
from datetime import datetime, date
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
import hearings.models as hmodels
import hearings.forms as hforms
from pyhearing.views import _is_teacher, _is_parent


@login_required
def recap(request):
    if not request.user.is_superuser:
        return redirect('home')
    requested_date = date.today()
    appointments = hmodels.Appointment.objects.filter(when__day=requested_date)
    ctx = dict(
        requested_date=requested_date,
        appointments=appointments
        )
    return render(request, 'hearings/recap.html', ctx)

@login_required
def personal_page(request):
    if _is_teacher(request.user) and not _is_parent(request.user):
        return redirect('teacher')
    if not _is_teacher(request.user) and _is_parent(request.user):
        return redirect('parent')
    return render(request, 'hearings/personal_page.html')


@login_required
def teacher(request):
    if not _is_teacher(request.user):
        messages.error(request, _("You are not a teacher, redirecting to your personal page"))
        return redirect('personal_page')
    availabilities = hmodels.Availability.objects.filter(teacher=request.user).filter(day__gte=date.today())
    appointments = hmodels.Appointment.objects.filter(teacher__user=request.user).filter(when__day__gte=date.today())
    return render(request, 'hearings/teacher.html', {
        'availabilities': availabilities,
        'appointments': appointments,
    })


@login_required
def parent(request):
    if not _is_parent(request.user):
        messages.error(request, _("You are not a parent, redirecting to your personal page"))
        return redirect('personal_page')
    parent = hmodels.Parent.objects.filter(user=request.user)
    if parent.count() < 1:
        return redirect('home')
    parent = parent[0]
    appointments = hmodels.Appointment.objects.filter(parent__user=request.user).filter(when__day__gte=date.today())
    classes = hmodels.SchoolClass.objects.filter(parents__user=request.user)
    qs = hmodels.Teacher.objects.filter(school_class__in=classes)
    teachers = []
    for teacher in qs:
        if teacher.can_add_appointment():
            teachers.append(teacher)
    return render(request, 'hearings/parent.html', {
        'parent': parent,
        'appointments': appointments,
        'classes': classes,
        'teachers': teachers,
    })


@login_required
def single_availability(request, id=None):
    if not _is_teacher(request.user):
        messages.error(request, _("You are not a teacher, redirecting to your personal page"))
        return redirect('personal_page')
    if id:
        instance = get_object_or_404(hmodels.Availability, pk=int(id))
        # Check that this is YOUR availability
        if instance.teacher != request.user:
            messages.error(request, _("You cannot edit another teacher's availability!"))
            return redirect('teacher')
    else:
        instance = None
    if request.method == 'POST':
        form = hforms.SingleAvailabilityForm(request.POST, instance=instance)
        if form.is_valid():
            availability = form.save(commit=False)
            availability.teacher = request.user
            availability.save()
            if instance:
                # We have modified an availability
                messages.info(request, _('Availability successfully modified'))
            else:
                # We created a new availability
                messages.info(request, _('Availability successfully created'))
            return redirect('teacher')
    else:
        form = hforms.SingleAvailabilityForm(instance=instance)
    return render(request, 'hearings/single_availability.html', {
        'form': form,
    })


def get_all_weekdays(my_day, day_from, day_to):
    # http://stackoverflow.com/a/2003906 with a little modification to
    # avoid to go backwards to the previous week..
    if int(my_day) < day_from.weekday():
        day_from += timedelta(days=7)
    day_from += timedelta(days=int(my_day) - day_from.weekday())
    while day_from <= day_to:
        yield day_from
        day_from += timedelta(days=7)


@login_required
def global_availabilities(request):
    if not _is_teacher(request.user):
        messages.error(request, _("You are not a teacher, redirecting to your personal page"))
        return redirect('personal_page')
    if request.method == 'POST':
        form = hforms.GlobalAvailabilitiesForm(request.POST)
        if form.is_valid():
            # Generating all Availability objects
            first_day = form.cleaned_data['first_day']
            last_day = form.cleaned_data['last_day']
            my_day = form.cleaned_data['day']
            all_days = get_all_weekdays(my_day, first_day, last_day)
            start_hour = form.cleaned_data['start']
            end_hour = form.cleaned_data['end']
            max_visits = form.cleaned_data['max_visits']
            how_many = 0
            for this_weekday in all_days:
                this_availability = hmodels.Availability(
                    teacher=request.user,
                    day=this_weekday,
                    start=start_hour,
                    end=end_hour,
                    max_visits=max_visits,
                )
                this_availability.save()
                how_many += 1
            messages.info(request, u"%d %s" % (how_many, _('availabilities successfully created')))
            return redirect('teacher')
    else:
        form = hforms.GlobalAvailabilitiesForm()
    return render(request, 'hearings/global_availabilities.html', {
        'form': form,
    })


@login_required
def delete_availability(request, id):
    if not _is_teacher(request.user):
        messages.error(request, _("You are not a teacher, redirecting to your personal page"))
        return redirect('personal_page')
    availability = get_object_or_404(hmodels.Availability, pk=int(id))
    # Check that this is YOUR availability
    if availability.teacher != request.user:
        messages.error(request, _("You cannot delete another teacher's availability!"))
        return redirect('teacher')
    # Delete appointments before deleting availability
    # Appointment model will take care of sending emails to parents and/or
    # teacher
    for appointment in availability.appointments.all():
        appointment.delete()
    availability.delete()
    return redirect('teacher')


@login_required
def book_appointment(request, id):
    if not _is_parent(request.user):
        messages.error(request, _("You are not a parent, redirecting to your personal page"))
        return redirect('personal_page')
    parent = hmodels.Parent.objects.filter(user=request.user)
    if parent.count() > 0:
        parent = parent[0]
    else:
        return redirect('home')

    classes = hmodels.SchoolClass.objects.filter(parents__user=request.user)
    qs = hmodels.Teacher.objects.filter(school_class__in=classes)
    teachers = []
    for teacher in qs:
        if teacher.can_add_appointment():
            teachers.append(teacher)

    teacher = get_object_or_404(hmodels.Teacher, pk=int(id))
    if teacher not in teachers:
        messages.error(request, _("You cannot book an appointment with this teacher"))
        return redirect('personal_page')
        
    # Get all availabilities for this teacher, starting from tomorrow
    availabilities = hmodels.Availability.objects.filter(teacher=teacher.user).filter(day__gt=date.today())
    return render(request, 'hearings/book_appointment.html', {
        'parent': parent,
        'teacher': teacher,
        'availabilities': availabilities,
    })


@login_required
def save_appointment(request, tid, aid):
    if not _is_parent(request.user):
        messages.error(request, _("You are not a parent, redirecting to your personal page"))
        return redirect('personal_page')
    teacher = get_object_or_404(hmodels.Teacher, pk=int(tid))
    availability = get_object_or_404(hmodels.Availability, pk=int(aid))
    if availability.can_add_appointment():
        school_class = teacher.school_class
        parent = get_object_or_404(hmodels.Parent, user=request.user, school_class=school_class)
        appointment = hmodels.Appointment(
            teacher=teacher,
            parent=parent,
            when=availability
        )
        appointment.save()
        messages.info(request, _("Appointment successfully saved"))
    return redirect('personal_page')


@login_required
def cancel_appointment(request, id):
    if not _is_parent(request.user):
        messages.error(request, _("You are not a parent, redirecting to your personal page"))
        return redirect('personal_page')
    appointment = get_object_or_404(hmodels.Appointment, pk=int(id))
    # Check that this is YOUR appointment
    if appointment.parent.user != request.user:
        messages.error(request, _("You cannot cancel another parent's appointment!"))
        return redirect('parent')
    appointment.delete()
    messages.info(request, _("Appointment successfully cancelled"))
    return redirect('personal_page')
