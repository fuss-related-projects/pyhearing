from fabric.api import local, run, sudo, cd, env
import os.path

env.hosts = ["natalia.fuss.bz.it"]

def deploy():
    base_path = "/var/www/udienze/"
    dirs = ["pyhearing", "torricelli"]
    for d in dirs:
        deploy_dir = os.path.join(base_path, d)
        with cd(deploy_dir):
            sudo("git pull")
            sudo("./manage.py syncdb")
            sudo("./manage.py collectstatic --noinput")
            sudo("./manage.py compilemessages")
        sudo("service apache2 restart")
        
